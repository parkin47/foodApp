import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox, QLabel-+

def show_groceries():
    pass

def create_food_menu(w):
    pass

def create_grocery_button(w):
    grocery_button = QPushButton(w)
    grocery_button.setFixedSize(150, 60)
    grocery_button.move(600, 120)
    grocery_button.setText("View Groceries")
    grocery_button.clicked.connect(show_groceries)

def window():
    app = QApplication(sys.argv)
    w = QWidget()
    w.resize(800,800)
    w.setWindowTitle("Food App")
    create_grocery_button(w)
    create_food_menu(w)

    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    window()